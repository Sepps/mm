cmake_minimum_required(VERSION 3.26)
project(MmDemo VERSION 0.1.0 LANGUAGES CXX)

add_executable(Mm_demo main.cpp)
target_compile_features(Mm_demo PRIVATE cxx_std_20)
target_link_libraries(Mm_demo PRIVATE Mm::lib)
add_executable(Mm::demo ALIAS Mm_demo)
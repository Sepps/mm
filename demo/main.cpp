#include "Mm/MmClifford.hpp"
#include "Mm/MmCompare.hpp"
#include "Mm/MmIostream.hpp"
#include <iostream>

int main()
{
	using Mm::e;
	auto a = e<1>;
	auto b = e<2>;
	auto c = e<3>;
	std::cout << "With the following basis vectors:\n";
	std::cout << "\ta:\t\t" << a << "\n";
	std::cout << "\tb:\t\t" << b << "\n";
	std::cout << "\tc:\t\t" << c << "\n";
	std::cout << "The result is embedded in the type:\n";
	std::cout << "\t(a+b)*c+(c+b):\t" << ((a+b)*c+(c+b)) << "\n";
}
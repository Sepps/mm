#pragma once
#include "Mm/MmCommon.hpp"
#include "Mm/MmOps.hpp"
#include "Mm/MmVector.hpp"

namespace Mm
{
	// u ∧ u = 0
	// u ∧ v = - v ∧ u (anti-commutative, each has the opposite orientation)
	template <Scalar T, sz N>
	struct Bivector
	{
		static constexpr bool associative = true;
		static constexpr bool commutative = false;
		static constexpr bool anti_commutative = true;
		static constexpr auto element_count = N;

		T elems[N] = {};

		[[nodiscard]] constexpr T & operator[]( sz i ) noexcept
		{
			return elems[i];
		}
		[[nodiscard]] constexpr T const & operator[]( sz i ) const noexcept
		{
			return elems[i];
		}
		[[nodiscard]] constexpr T SumOfElements() const noexcept
		{
			return [&]<sz... Is>( std::index_sequence<Is...> )
			{
				return ( elems[Is] + ... );
			}( std::make_index_sequence<N>() );
		}

		[[nodiscard]] friend constexpr T Dot( Bivector const & lhs, Bivector const & rhs ) noexcept
		{
			return ElementwiseOp<Bivector, mul_op>( lhs, rhs ).SumOfElements();
		}

		friend constexpr Bivector operator-( Bivector const & lhs, Bivector const & rhs ) noexcept
		{
			return ElementwiseOp<Bivector, sub_op>( lhs, rhs );
		}

		friend constexpr Bivector operator+( Bivector const & lhs, Bivector const & rhs ) noexcept
		{
			return ElementwiseOp<Bivector, add_op>( lhs, rhs );
		}
	};
}
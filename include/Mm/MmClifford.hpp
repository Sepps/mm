#pragma once
#include "Mm/MmCommon.hpp"
#include "Mm/MmCompare.hpp"

#include <algorithm>
#include <array>
#include <functional>
#include <span>
#include <type_traits>
#include <utility>

namespace Mm
{
	template <int mul, sz... Ds>
	struct Mul
	{
		static constexpr auto multiple = mul;
		static constexpr auto elems = std::array<sz, sizeof...( Ds )>{ Ds... };
	};

	template <sz D>
	using E = Mul<+1, D>;

	template <sz D>
	inline constexpr auto e = E<D>{};

	template <sz A, sz B>
	[[nodiscard]] constexpr bool operator<( E<A> const &, E<B> const & ) noexcept
	{
		return A < B;
	}

	template <typename>
	struct MulInv;
	template <int mult, sz... Ds>
	struct MulInv<Mul<mult, Ds...>>
	{
		using type = Mul<-mult, Ds...>;
	};

	template <typename T>
	using get_mul_inv = typename MulInv<T>::type;

	struct ElemsSpan
	{
		std::span<sz const> elems;
	};

	struct MulErased
	{
		int multiple = 0;
		ElemsSpan elems = {};

		constexpr MulErased() = default;
		template <int multiple_, sz... Ds>
		constexpr MulErased( Mul<multiple_, Ds...> const & muls ) noexcept
			: multiple( muls.multiple )
			, elems( muls.elems )
		{
		}
	};

	template <int multiply, sz... Es>
	inline constexpr auto mul = Mul<multiply, Es...>{};

	template <int multiply, std::array items>
	inline constexpr auto mul_from_array = ([]<sz... Is>(std::index_sequence<Is...>) {
      return mul<multiply, items[Is]...>;
    }(std::make_index_sequence<items.size()>()));

	template <typename T>
	inline constexpr bool is_mul_v = false;
	template <int mul, sz... Ds>
	inline constexpr bool is_mul_v<Mul<mul, Ds...>> = true;
	template <typename T>
	concept IsMul = is_mul_v<std::remove_cvref_t<T>>;

	template <auto Es>
	class MulSortimpl
	{
		template <sz N>
		struct SortedResultType
		{
			int multiple = 0;
			std::array<sz, N> elems = {};
		};

		static constexpr auto sorted_es_result = ( []() constexpr -> SortedResultType<Es.elems.size()> {
			auto output = SortedResultType<Es.elems.size()>{ Es.multiple, Es.elems };
			output.multiple = ( ( BubbleSort( output.elems ) % 2 ) == 0 ) ? +1 : -1;
			return output;
		}() );
	public:
		static constexpr auto value = mul_from_array<Es.multiple * sorted_es_result.multiple, sorted_es_result.elems>;
	};

	template <auto Es>
	class MulCancelImpl
	{
		static constexpr auto handle_list = ([]( auto const & elems, auto push ) noexcept
		{
			for ( sz i = 0; i < Es.elems.size(); )
			{
				auto const curr = Es.elems[i];
				sz j = i + 1;
				for ( ; j < Es.elems.size(); ++j )
				{
					auto const next = Es.elems[j];
					if ( curr != next ) { break; }
				}
				sz const len = j - i;
				if ( ( len % 2 ) == 1 ) { push( curr ); }
				i += len;
			}
		});

		static constexpr auto unique_count = ([]() noexcept -> sz {
			sz count = 0;
			handle_list(Es.elems, [&](sz) { ++count; });
			return count;
		}());

		static constexpr auto unique_mul_list = ([]() noexcept -> std::array<sz, unique_count> {
			std::array<sz, unique_count> output{};
			auto const push_elems = [&, index = sz{}](sz value) mutable { output[index++] = value; };
			handle_list(Es.elems, push_elems);
			return output;
		}());

	public:
		static constexpr auto value = mul_from_array<Es.multiple, unique_mul_list>;
	};

	template <auto Es>
	inline constexpr auto mul_sort = MulSortimpl<Es>::value;

	template <auto Es>
	inline constexpr auto mul_cancel = MulCancelImpl<Es>::value;

	template <auto Es>
	inline constexpr auto mul_simplify = mul_cancel<mul_sort<Es>>;

	template <IsMul... Ts>
	struct Add
	{
	};

	template <typename T>
	struct InvFrontAdd
	{
		using type = T;
	};
	template <IsMul F, IsMul... Ts>
	struct InvFrontAdd<Add<F, Ts...>>
	{
		using type = Add<get_mul_inv<F>, Ts...>;
	};
	template <typename T>
	using get_inv_front_add = typename InvFrontAdd<T>::type;

	template <IsMul... Ts>
	[[nodiscard]] constexpr auto InvSign( Add<Ts...> const & ) -> get_inv_front_add<Add<Ts...>>
	{
		return {};
	}
	template <int mul, sz... Ds>
	[[nodiscard]] constexpr auto InvSign( Mul<mul, Ds...> const & ) -> Mul<-mul, Ds...>
	{
		return {};
	}

	template <typename>
	class SortAdd;
	template <IsMul... Ts>
	class SortAdd<Add<Ts...>>
	{
		using raw_sorted_erased_muls_type = std::array<MulErased, sizeof...( Ts )>;
		static constexpr auto raw_sorted_erased_muls = []() -> raw_sorted_erased_muls_type
		{
			auto output = raw_sorted_erased_muls_type{ MulErased{ Ts{} }... };
			std::sort( output.begin(), output.end() );
			return output;
		}();

		static constexpr sz unique_erased_muls = []() -> sz
		{
			auto const & elems = raw_sorted_erased_muls;
			sz count = 0;
			if ( elems.size() > 0 )
			{
				++count;
				for ( sz i = 1; i < elems.size(); ++i )
				{
					if ( elems[i - 1].elems != elems[i].elems ) ++count;
				}
			}
			return count;
		}();

		using sorted_erased_muls_type = std::array<MulErased, unique_erased_muls>;
		static constexpr auto sorted_erased_muls = []() -> sorted_erased_muls_type
		{
			sorted_erased_muls_type output;
			if ( raw_sorted_erased_muls.size() > 0 )
			{
				sz outi = 0;
				output[outi] = raw_sorted_erased_muls[0];
				for ( sz i = 1; i < raw_sorted_erased_muls.size(); ++i )
				{
					auto const & a = raw_sorted_erased_muls[i - 1];
					auto const & b = raw_sorted_erased_muls[i];
					if ( a.elems == b.elems ) { output[outi].multiple += b.multiple; }
					else { output[++outi] = b; }
				}
			}
			return output;
		}();

		static constexpr auto is_non_zero( MulErased const & value ) noexcept
		{
			return value.multiple != 0;
		}
		static constexpr auto non_zero_count = std::count_if( sorted_erased_muls.begin(), sorted_erased_muls.end(), is_non_zero );

		using non_zero_sorted_erased_muls_type = std::array<MulErased, non_zero_count>;
		static constexpr auto non_zero_sorted_erased_muls = []() -> non_zero_sorted_erased_muls_type
		{
			non_zero_sorted_erased_muls_type output;
			sz i = 0;
			for ( auto const & v : sorted_erased_muls )
			{
				if ( v.multiple != 0 ) { output[i++] = v; }
			}
			return output;
		}();

		template <sz I>
		static constexpr auto sorted_multiple = non_zero_sorted_erased_muls[I].multiple;

		template <sz I>
		static constexpr auto sorted_elems = []<sz... Is>( std::index_sequence<Is...> ) -> std::array<sz, sizeof...( Is )>
		{
			return { sz( non_zero_sorted_erased_muls[I].elems.elems[Is] )... };
		}
		( std::make_index_sequence<non_zero_sorted_erased_muls[I].elems.elems.size()>() );

		template <sz I>
		using sorted_mul = std::remove_cvref_t<decltype( Mm::mul_from_array<sorted_multiple<I>, sorted_elems<I>> )>;
	public:
		static constexpr auto value = []<sz... Is>( std::index_sequence<Is...> ) noexcept -> Add<sorted_mul<Is>...>
		{
			return {};
		}( std::make_index_sequence<non_zero_count>() );
	};

	template <auto Es>
	inline auto constexpr add_sort = SortAdd<std::remove_cvref_t<decltype( Es )>>::value;

	template <auto Es>
	inline auto constexpr add_simplify = add_sort<Es>;
	[[nodiscard]] constexpr bool operator<( ElemsSpan const & lhs, ElemsSpan const & rhs ) noexcept
	{
		return std::lexicographical_compare( lhs.elems.begin(), lhs.elems.end(), rhs.elems.begin(), rhs.elems.end(), std::less<sz>{} );
	}
	[[nodiscard]] constexpr bool operator>( ElemsSpan const & lhs, ElemsSpan const & rhs ) noexcept
	{
		return rhs < lhs;
	}
	[[nodiscard]] constexpr bool operator>=( ElemsSpan const & lhs, ElemsSpan const & rhs ) noexcept
	{
		return not( lhs < rhs );
	}
	[[nodiscard]] constexpr bool operator<=( ElemsSpan const & lhs, ElemsSpan const & rhs ) noexcept
	{
		return not( lhs > rhs );
	}
	[[nodiscard]] constexpr bool operator==( ElemsSpan const & lhs, ElemsSpan const & rhs ) noexcept
	{
		return std::equal( lhs.elems.begin(), lhs.elems.end(), rhs.elems.begin(), rhs.elems.end() );
	}
	[[nodiscard]] constexpr bool operator!=( ElemsSpan const & lhs, ElemsSpan const & rhs ) noexcept
	{
		return not std::equal( lhs.elems.begin(), lhs.elems.end(), rhs.elems.begin(), rhs.elems.end() );
	}

	[[nodiscard]] constexpr bool operator<( MulErased const & lhs, MulErased const & rhs ) noexcept
	{
		return lhs.elems < rhs.elems;
	}
	[[nodiscard]] constexpr bool operator>( MulErased const & lhs, MulErased const & rhs ) noexcept
	{
		return rhs < lhs;
	}
	[[nodiscard]] constexpr bool operator>=( MulErased const & lhs, MulErased const & rhs ) noexcept
	{
		return not( lhs < rhs );
	}
	[[nodiscard]] constexpr bool operator<=( MulErased const & lhs, MulErased const & rhs ) noexcept
	{
		return not( lhs > rhs );
	}
	[[nodiscard]] constexpr bool operator==( MulErased const & lhs, MulErased const & rhs ) noexcept
	{
		return lhs.multiple == rhs.multiple and lhs.elems == rhs.elems;
	}
	[[nodiscard]] constexpr bool operator!=( MulErased const & lhs, MulErased const & rhs ) noexcept
	{
		return not( lhs == rhs );
	}

	template <IsMul Lhs, IsMul Rhs>
	[[nodiscard]] constexpr auto operator+( Lhs const &, Rhs const & ) noexcept
	{
		constexpr auto value = Add<Lhs, Rhs>{};
		return add_simplify<value>;
	}
	template <IsMul Lhs, IsMul... Rs>
	[[nodiscard]] constexpr auto operator+( Lhs const &, Add<Rs...> const & ) noexcept
	{
		constexpr auto value = Add<Lhs, Rs...>{};
		return add_simplify<value>;
	}
	template <IsMul... Rs, IsMul Rhs>
	[[nodiscard]] constexpr auto operator+( Add<Rs...> const &, Rhs const & ) noexcept
	{
		constexpr auto value = Add<Rs..., Rhs>{};
		return add_simplify<value>;
	}
	template <IsMul... Das, IsMul... Dbs>
	[[nodiscard]] constexpr auto operator+( Add<Das...> const &, Add<Dbs...> const & ) noexcept
	{
		constexpr auto value = Add<Das..., Dbs...>{};
		return add_simplify<value>;
	}

	template <IsMul Lhs>
	[[nodiscard]] constexpr auto operator-( Lhs const & lhs ) noexcept
	{
		return InvSign( lhs );
	}
	template <IsMul Lhs, IsMul Rhs>
	[[nodiscard]] constexpr auto operator-( Lhs const & lhs, Rhs const & rhs ) noexcept
	{
		return lhs + InvSign( rhs );
	}
	template <IsMul Lhs, IsMul... Rs>
	[[nodiscard]] constexpr auto operator-( Lhs const & lhs, Add<Rs...> const & rhs ) noexcept
	{
		return lhs + InvSign( rhs );
	}
	template <IsMul... Rs, IsMul Rhs>
	[[nodiscard]] constexpr auto operator-( Add<Rs...> const & lhs, Rhs const & rhs ) noexcept
	{
		return lhs + InvSign( rhs );
	}
	template <IsMul... Das, IsMul... Dbs>
	[[nodiscard]] constexpr auto operator-( Add<Das...> const & lhs, Add<Dbs...> const & rhs ) noexcept
	{
		return lhs + InvSign( rhs );
	}

	template <typename L, typename R>
	struct Multiply;
	template <int multiple_a, int multiple_b, sz... As, sz... Bs>
	struct Multiply<Mul<multiple_a, As...>, Mul<multiple_b, Bs...>>
	{
		using type = Mul<multiple_a * multiple_b, As..., Bs...>;
	};
	template <typename L, typename R>
	using get_multiply_type = std::remove_cvref_t<decltype( mul_simplify<typename Multiply<L, R>::type{}> )>;

	template <int multiple_a, int multiple_b, sz... As, sz... Bs>
	[[nodiscard]] constexpr auto operator*( Mul<multiple_a, As...> const & lhs, Mul<multiple_b, Bs...> const & rhs ) noexcept -> get_multiply_type<Mul<multiple_a, As...>, Mul<multiple_b, Bs...>>
	{
		return {};
	}
	template <IsMul Lhs, IsMul... Dbs>
	[[nodiscard]] constexpr auto operator*( Lhs const & lhs, Add<Dbs...> const & rhs ) noexcept -> decltype(add_simplify<Add<get_multiply_type<Lhs, Dbs>...>{}>)
	{
		return {};
	}
	template <IsMul... Dbs, IsMul Rhs>
	[[nodiscard]] constexpr auto operator*( Add<Dbs...> const & lhs, Rhs const & rhs ) noexcept -> decltype(add_simplify<Add<get_multiply_type<Dbs, Rhs>...>{}>)
	{
		return {};
	}
	template <IsMul... Lhs, IsMul... Rhs>
	[[nodiscard]] constexpr auto operator*( Add<Lhs...> const & lhs, Add<Rhs...> const & rhs ) noexcept
	{
		return ( ( Lhs{} * rhs ) + ... );
	}
}
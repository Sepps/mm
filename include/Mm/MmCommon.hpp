#pragma once
#include <array>
#include <type_traits>

namespace Mm
{
	using sz = decltype( sizeof( int ) );
	using f32 = float;
	using f64 = double;

	namespace Impl
	{
		template <template <typename...> class Match, typename List>
		struct TLOfTypeImpl : std::false_type
		{
		};
		template <template <typename...> class Match, typename... Args>
		struct TLOfTypeImpl<Match, Match<Args...>> : std::true_type
		{
		};
	}

	template <typename List, template <typename...> class Match>
	concept TLOfType = Impl::TLOfTypeImpl<Match, List>::value;

	template <typename T, sz N>
	[[nodiscard]] constexpr int BubbleSort( std::array<T, N> & array ) noexcept
	{
		int swaps = 0;
		for ( int step = 0; step < ( N - 1 ); ++step )
		{
			int swapped = 0;
			for ( int i = 0; i < ( N - step - 1 ); ++i )
			{
				if ( array[i] > array[i + 1] )
				{
					int temp = array[i];
					array[i] = array[i + 1];
					array[i + 1] = temp;
					swapped = 1;
					++swaps;
				}
			}
			if ( swapped == 0 ) break;
		}
		return swaps;
	}
}
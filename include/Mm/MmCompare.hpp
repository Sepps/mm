#pragma once
#include <tuple>
#include <algorithm>
#include <functional>

namespace Mm
{
	template <typename ... Lhs, typename ... Rhs, typename Compare = std::less<void>>
	[[nodiscard]] constexpr bool LexCompare(std::tuple<Lhs...> const & lhs, std::tuple<Rhs...> const & rhs, Compare comp = {}) noexcept
	{
		constexpr auto min_elems = std::min(sizeof...(Lhs), sizeof...(Rhs));
		auto const compare_result = [&]<std::size_t...Is>(std::index_sequence<Is...>) noexcept
		{
			auto looper = [&](auto const & lhs_elem, auto const & rhs_elem) noexcept -> int
			{
				if (comp(lhs_elem, rhs_elem)) return 1;
				if (comp(rhs_elem, lhs_elem)) return -1;
				return 0;
			};
			using std::get;
			int result = 0;
			(void)(((result = looper(get<Is>(lhs), get<Is>(rhs))) == 0) and ...);
			return result;
		}(std::make_index_sequence<min_elems>());
		return ((compare_result == 0) ? (sizeof...(Rhs) > sizeof...(Lhs)) : compare_result == 1);
	}
}
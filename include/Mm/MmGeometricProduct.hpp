#pragma once
#include "Mm/MmBivector.hpp"
#include "Mm/MmVector.hpp"
#include "Mm/MmWedgeProduct.hpp"

namespace Mm
{
	// TODO: this is somewhat too specific to vector-vector geometric product
	template <Scalar T, sz N>
	struct GeometricProductResult
	{
		static constexpr bool associative = true;
		static constexpr bool commutative = false;
		static constexpr bool anti_commutative = false;

		T dot_product;                   // grade 0 object.
		Bivector<T, N> wedge_product;    // grade 2 object.
	};

	template <Scalar T, sz N>
	[[nodiscard]] constexpr auto GeometricProduct( Bivector<T, N> const & wedge_product, T dot_product ) -> GeometricProductResult<T, N>
	{
		return {
			.dot_product = dot_product,
			.wedge_product = wedge_product,
		};
	}

	template <Scalar T, sz N>
	[[nodiscard]] constexpr auto GeometricProduct( Vector<T, N> const & lhs, Vector<T, N> const & rhs ) noexcept -> GeometricProductResult<T, N>
	{
		return {
			.dot_product = Dot( lhs, rhs ),
			.wedge_product = WedgeProduct( lhs, rhs ),
		};
	}
}
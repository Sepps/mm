#pragma once
#include "Mm/MmVector.hpp"
#include "Mm/MmMatrix.hpp"
#include "Mm/MmBivector.hpp"
#include "Mm/MmGeometricProduct.hpp"
#include "Mm/MmClifford.hpp"

#include <string>
#include <ostream>
#include <iostream>
#include <string_view>
#include <vector>
#include <numeric>
#include <cstdlib>
#include <memory>
#include <typeinfo>

template <Mm::Scalar T, Mm::sz N>
inline std::ostream & operator<<(std::ostream & o, Mm::Vector<T, N> const & rhs)
{
	std::string output;
	for (auto const & elem : rhs.elems)
	{
		output += std::to_string(elem) + ", ";
	}
	if (not output.empty())
	{
		output.pop_back();
		output.pop_back();
	}
	return o << "<" << output << ">";
}

template <Mm::Scalar T, Mm::sz N>
inline std::ostream & operator<<(std::ostream & o, Mm::Bivector<T, N> const & rhs)
{
	std::string output;
	for (auto const & elem : rhs.elems)
	{
		output += std::to_string(elem) + ", ";
	}
	if (not output.empty())
	{
		output.pop_back();
		output.pop_back();
	}
	return o << "(" << output << ")";
}

template <Mm::Scalar T, Mm::sz N>
inline std::ostream & operator<<(std::ostream & o, Mm::GeometricProductResult<T, N> const & rhs)
{
	std::string output;
	for (auto const & elem : rhs.wedge_product.elems)
	{
		output += std::to_string(elem) + ", ";
	}
	if (not output.empty())
	{
		output.pop_back();
		output.pop_back();
	}
	return o << "(" << rhs.dot_product << ", " << output << ")";
}

template <Mm::Scalar T, Mm::sz H, Mm::sz W>
inline std::ostream & operator<<(std::ostream & o, Mm::Matrix<T, H, W> const & rhs)
{
	std::array<std::array<std::string, W>, H> rows;
	for (Mm::sz x = 0; x < W; ++x) { for (Mm::sz y = 0; y < H; ++y) { rows[y][x] = std::to_string(rhs(x, y)); } }
	std::array<Mm::sz, W> widths{};
	for (Mm::sz x = 0; x < W; ++x) { for (Mm::sz y = 0; y < H; ++y) { widths[x] = std::max(widths[x], rows[y][x].size()); } }
	for (Mm::sz x = 0; x < W; ++x)
	{
		auto const max_width = widths[x];
		for (Mm::sz y = 0; y < H; ++y)
		{
			auto & row = rows[y][x];
			row.insert(0, max_width - row.size(), ' ');
		}
	}
	std::string output;
	for (Mm::sz y = 0; y < H; ++y)
	{
		output += "| ";
		for (Mm::sz x = 0; x < W; ++x)
		{
			output += rows[y][x] + " ";
		}
		output += "|\n";
	}
	return o << output;
}

template <typename T>
concept UnsignedIntegral = (std::is_integral_v<T> and std::is_unsigned_v<T>);
template <UnsignedIntegral T>
struct UnsignedSubscript { T value; };
template <UnsignedIntegral T>
struct UnsignedSuperscript { T value; };

template <UnsignedIntegral T>
inline std::string ToU8String(UnsignedSubscript<T> const & value)
{
	std::u8string_view lut[]{ u8"₀", u8"₁", u8"₂", u8"₃", u8"₄", u8"₅", u8"₆", u8"₇", u8"₈", u8"₉" };
	std::u8string output;
	auto temp = std::to_string(value.value);
	for (auto const & v : temp) { output += lut[v-'0']; }
	return { reinterpret_cast<const char *>(output.data()), output.size() };
}

template <UnsignedIntegral T>
inline std::string ToU8String(UnsignedSuperscript<T> const & value)
{
	std::u8string_view lut[]{ u8"⁰", u8"¹", u8"²", u8"³", u8"⁴", u8"⁵", u8"⁶", u8"⁷", u8"⁸", u8"⁹" };
	std::u8string output;
	auto temp = std::to_string(value.value);
	for (auto const & v : temp) { output += lut[v-'0']; }
	return { reinterpret_cast<const char *>(output.data()), output.size() };
}

#ifdef _WIN32
#include <Windows.h>
struct EnableUtf8
{
	EnableUtf8()
	{
		SetConsoleOutputCP(CP_UTF8);
		SetConsoleCP(CP_UTF8);
	}
};
#else
struct EnableUtf8{};
#endif

template <int sign, Mm::sz...Ds>
inline std::ostream & operator<<(std::ostream & o, Mm::Mul<sign, Ds...> const & rhs)
{
	static EnableUtf8 enable_utf8;
	int sign_value = sign;
	if (sign_value == -1) { o << "-"; sign_value = -sign_value; }
	if (sign_value > 1 or sizeof...(Ds) == 0) { o << sign_value; }
	(
		(o << "e" << ToU8String(UnsignedSubscript<Mm::sz>{ .value = Ds })),
		...
	);
	return o;
}

#include <sstream>

template <Mm::IsMul...Ms>
inline std::ostream & operator<<(std::ostream & o, Mm::Add<Ms...> const & rhs)
{
	auto const replace_string = [](std::string subject, const std::string& search, const std::string& replace) {
		size_t pos = 0;
		while ((pos = subject.find(search, pos)) != std::string::npos) {
			subject.replace(pos, search.length(), replace);
			pos += replace.length();
		}
		return subject;
	};
	std::stringstream ss;
	((ss << Ms{} << "+"), ...);
	auto temp = replace_string(replace_string(ss.str(), "+-", "-"), "--", "+");
	if (temp.size() > 0)
	{
		temp.pop_back();
	}
	return o << temp;
}

#include <cxxabi.h> 

inline std::ostream & operator<<(std::ostream & o, std::type_info const & rhs)
{
    int status = -4;
    std::unique_ptr<char, void(*)(void*)> res{ abi::__cxa_demangle(rhs.name(), NULL, NULL, &status), std::free };
    std::string string = (status==0) ? res.get() : rhs.name();
	return o << string;
}

template <typename T>
struct TypeTaggedValue { T const & value; };
template <typename T>
TypeTaggedValue(T)->TypeTaggedValue<T>;

template <typename T>
inline std::ostream & operator<<(std::ostream & o, TypeTaggedValue<T> const & rhs)
{
	return (o << "{ \"" << typeid( std::remove_cvref_t<T> ) << "\" : \"" << rhs.value << "\" }");
}

template <typename T, Mm::sz N>
inline std::ostream & operator<<(std::ostream & o, std::array<T, N> const & arr)
{
	std::string output;
	std::stringstream ss;
	for (auto const & v : arr)
	{
		ss << v << ",";
	}
	auto temp = ss.str();
	temp.pop_back();
	return o << "[" << temp << "]";
}

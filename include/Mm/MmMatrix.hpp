#pragma once
#include "Mm/MmCommon.hpp"
#include "Mm/MmScalar.hpp"
#include "Mm/MmOps.hpp"
#include <array>
#include <initializer_list>
#include <type_traits>

namespace Mm
{
	template <Scalar T>
	[[nodiscard]] constexpr auto Det2x2(T a, T b, T c, T d) noexcept
	{
		return a * d - c * b;
	}

	template <Scalar T, sz H, sz W, typename = std::make_index_sequence<H * W>>
	struct MatrixBase;

	template <Scalar T, sz H, sz W, sz ... Is>
	struct MatrixBase<T, H, W, std::index_sequence<Is...>>
	{
		static constexpr sz element_count = H * W;
		T elems[element_count] = {};

		[[nodiscard]] static constexpr sz GetRow(sz i) noexcept { return i / W; }
		[[nodiscard]] static constexpr sz GetCol(sz i) noexcept { return i % W; }

		constexpr MatrixBase(std::initializer_list<std::initializer_list<T>> rows) noexcept
			: elems{ (rows.begin()[GetRow(Is)].begin()[GetCol(Is)])... }
		{}
		constexpr MatrixBase(std::initializer_list<T> elems) noexcept
			: elems{ elems.begin()[Is]... }
		{}
	};

	template <Scalar T, sz H, sz W = H> // default to square matrix
	struct Matrix : MatrixBase<T, H, W>
	{
		using base = MatrixBase<T, H, W>;
		using base::base;
		using base::elems;
		using base::element_count;

		[[nodiscard]] constexpr T & operator[](sz i) noexcept
		{
			return elems[i];
		}

		[[nodiscard]] constexpr T const & operator[](sz i) const noexcept
		{
			return elems[i];
		}

		[[nodiscard]] constexpr T & operator()(sz x, sz y) noexcept
		{
			return elems[y * W + x];
		}

		[[nodiscard]] constexpr T const & operator()(sz x, sz y) const noexcept
		{
			return elems[y * W + x];
		}

		[[nodiscard]] friend constexpr Matrix operator+(Matrix const & lhs, Matrix const & rhs) noexcept
		{
			return ElementwiseOp<Matrix, add_op>(lhs, rhs);
		}

		[[nodiscard]] friend constexpr Matrix operator-(Matrix const & lhs, Matrix const & rhs) noexcept
		{
			return ElementwiseOp<Matrix, sub_op>(lhs, rhs);
		}
	};
}
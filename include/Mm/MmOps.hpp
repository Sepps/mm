#pragma once
#include "Mm/MmCommon.hpp"

#include <type_traits>
#include <utility>

namespace Mm
{
	struct SubOp
	{
		[[nodiscard]] constexpr decltype( auto ) operator()( auto const & lhs, auto const & rhs ) const noexcept
		{
			return lhs - rhs;
		}
	};
	inline constexpr auto sub_op = SubOp{};

	struct AddOp
	{
		[[nodiscard]] constexpr decltype( auto ) operator()( auto const & lhs, auto const & rhs ) const noexcept
		{
			return lhs + rhs;
		}
	};
	inline constexpr auto add_op = AddOp{};

	struct MulOp
	{
		[[nodiscard]] constexpr decltype( auto ) operator()( auto const & lhs, auto const & rhs ) const noexcept
		{
			return lhs * rhs;
		}
	};
	inline constexpr auto mul_op = MulOp{};

	template <typename T, auto Op>
	[[nodiscard]] constexpr T ElementwiseOp( T const & lhs, T const & rhs ) noexcept
	{
		return [&]<sz... Is>( std::index_sequence<Is...> ) noexcept -> T
		{
			return T{ Op( lhs[Is], rhs[Is] )... };
		}( std::make_index_sequence<T::element_count>() );
	}
}
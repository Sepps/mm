#pragma once
#include "Mm/MmCommon.hpp"

#include <type_traits>

namespace Mm
{
	template <typename T>
	concept Scalar = std::is_arithmetic_v<T>;
}
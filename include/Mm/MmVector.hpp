#pragma once
#include "Mm/MmCommon.hpp"
#include "Mm/MmOps.hpp"
#include "Mm/MmScalar.hpp"

#include <cmath>
#include <numeric>


namespace Mm
{
	template <Scalar T, sz N>
	struct Vector
	{
		static constexpr sz element_count = N;

		T elems[N]{};

		[[nodiscard]] constexpr T & operator[]( sz i ) noexcept
		{
			return elems[i];
		}
		[[nodiscard]] constexpr T const & operator[]( sz i ) const noexcept
		{
			return elems[i];
		}

		[[nodiscard]] constexpr T SqrMagnitude() const noexcept
		{
			return [&]<sz... Is>( std::index_sequence<Is...> )
			{
				return ( ( elems[Is] * elems[Is] ) + ... );
			}( std::make_index_sequence<N>() );
		}

		[[nodiscard]] constexpr T Magnitude() const noexcept
		{
			return std::sqrt( SqrMagnitude() );
		}

		[[nodiscard]] constexpr T SumOfElements() const noexcept
		{
			return [&]<sz... Is>( std::index_sequence<Is...> )
			{
				return ( elems[Is] + ... );
			}( std::make_index_sequence<N>() );
		}

		friend constexpr Vector operator-( Vector const & lhs, Vector const & rhs ) noexcept
		{
			return ElementwiseOp<Vector, sub_op>( lhs, rhs );
		}

		friend constexpr Vector operator+( Vector const & lhs, Vector const & rhs ) noexcept
		{
			return ElementwiseOp<Vector, add_op>( lhs, rhs );
		}

		friend constexpr T Dot( Vector const & lhs, Vector const & rhs ) noexcept
		{
			return ElementwiseOp<Vector, mul_op>( lhs, rhs ).SumOfElements();
		}
	};
	template <Scalar... Ts>
	Vector( Ts... ) -> Vector<std::common_type_t<Ts...>, sizeof...( Ts )>;

	static_assert( Vector<int, 3>{ 3, 3, 3 }.SqrMagnitude() == 27, "The magnitude must be the square root of the sub of the squares" );
}
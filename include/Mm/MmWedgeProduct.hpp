#pragma once
#include "Mm/MmBivector.hpp"
#include "Mm/MmVector.hpp"
#include <array>

namespace Mm
{
	template <Scalar T, sz N>
	struct WedgeProductLut
	{
		using bivector_type = Bivector<T, N *( N - 1 ) / 2>;

	private:
		struct state
		{
			sz x;
			sz y;
		};
		static constexpr auto lut_length = N * ( N - 1 ) / 2;
		using lut_type = std::array<state, lut_length>;

		static constexpr lut_type value = []
		{
			lut_type output{};
			sz index = 0;
			for ( sz x = 0; x + 1 < N; ++x )
			{
				for ( sz y = x + 1; y < N; ++y )
				{
					output[index++] = {
						.x = x,
						.y = y,
					};
				}
			}
			return output;
		}();

		template <sz... Is>
		[[gnu::always_inline, nodiscard]] static constexpr auto Impl( Vector<T, N> const & lhs, Vector<T, N> const & rhs, std::index_sequence<Is...> ) noexcept -> bivector_type
		{
			return { Det2x2( lhs[value[Is].x], lhs[value[Is].y], rhs[value[Is].x], rhs[value[Is].y] )... };
		}

	public:
		[[gnu::always_inline, nodiscard]] static constexpr auto Impl( Vector<T, N> const & lhs, Vector<T, N> const & rhs ) noexcept -> bivector_type
		{
			return Impl( lhs, rhs, std::make_index_sequence<lut_length>() );
		}
	};

	template <Scalar T, sz N>
	[[nodiscard]] constexpr auto WedgeProduct( Vector<T, N> const & lhs, Vector<T, N> const & rhs ) noexcept -> typename WedgeProductLut<T, N>::bivector_type
	{
		return WedgeProductLut<T, N>::Impl( lhs, rhs );
	}
}
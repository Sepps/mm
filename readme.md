Mm
===

A geometric algebra library authored primarily to learn geometric algebra.

Features:
 - Geometric algebra is handled by the type system.
 - Wedge product.
 - Geometric product.
 - Addition and subtraction.
 - Basic implementation of Bivector.

Planned features:
 - Increase test coverage.
 - Complex numbers, quaternions etc...
 - Rotors.
 - Increase operations supported each of the above.

# Notes

## Generation of compile_commands.json

If you enable `CMAKE_EXPORT_COMPILE_COMMANDS`, the project will place the `compile_commands.json` into the build directory (And not the build/Debug directory). This is primarily so that the path to that file is constant, making setup in IDE's that use the file simpler.
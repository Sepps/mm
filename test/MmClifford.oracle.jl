using Pkg
Pkg.add(url="https://github.com/Jollywatt/GeometricAlgebra.jl", rev="v0.2.1")
using GeometricAlgebra

v = basis(3)

e1 = v[1]
e2 = v[2]
e3 = v[3]

a = e2 * e3
b = e1 * e2
c = e2 * e1

lhs = a * (a + b)
rhs = b * (a + b)

println(e1)
println(-e1)
println(-(-e1))
println(e1 + e1)
println(e1 + e2)
println(e1 - e1)
println(e1 * e1)
println(e2 * e2)
println(e3 * e3)
println(e1 * e2)
println(e2 * e1)
println(e1 * (e1 + e2))
println(e2 * (e1 + e2))
println(e2 * e1 + e2 * e2)
println(a * a)
println(b * b)
println(a * b)
println(b * a)
println(a * (a + b))
println(b * (a + b))
println((a + b) * a)
println((a + b) * b)
println((c + b) * c)
println((c + b) * b)
println(lhs + rhs)
println((a + b) * (a + b))
println(a * b)
println(a)
println(b)
println(c)
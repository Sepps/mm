#include "Mm/MmClifford.hpp"
#include "Mm/MmIostream.hpp"

namespace
{
	using Mm::e;
	auto to_string = [](auto const & v)
	{
		std::stringstream ss;
		ss << v;
		return ss.str();
	};

	constexpr auto e1 = e<1>;
	constexpr auto e2 = e<2>;
	constexpr auto e3 = e<3>;
	constexpr auto a = e2 * e3;
	constexpr auto b = e1 * e2;
	constexpr auto c = e2 * e1;
	constexpr auto lhs = a * (a + b);
	constexpr auto rhs = b * (a + b);
}

#include "catch2/catch_all.hpp"

TEST_CASE("ElemSpan can compare element spans", "[Mm]")
{
	SECTION("Same empty")
	{
		std::array<Mm::sz, 4> a{};
		std::array<Mm::sz, 4> b{};
		CHECK(Mm::ElemsSpan{ a } == Mm::ElemsSpan{ b });
		CHECK(Mm::ElemsSpan{ a } <= Mm::ElemsSpan{ b });
		CHECK(Mm::ElemsSpan{ a } >= Mm::ElemsSpan{ b });
		CHECK_FALSE(Mm::ElemsSpan{ a } != Mm::ElemsSpan{ b });
		CHECK_FALSE(Mm::ElemsSpan{ a } < Mm::ElemsSpan{ b });
		CHECK_FALSE(Mm::ElemsSpan{ a } > Mm::ElemsSpan{ b });
	}
	SECTION("Different size, non-zero")
	{
		std::array<Mm::sz, 4> a{1, 2, 3, 4};
		std::array<Mm::sz, 2> b{1, 2};
		CHECK(Mm::ElemsSpan{ a } != Mm::ElemsSpan{ b });
		CHECK(Mm::ElemsSpan{ a } > Mm::ElemsSpan{ b });
		CHECK(Mm::ElemsSpan{ a } >= Mm::ElemsSpan{ b });
		CHECK_FALSE(Mm::ElemsSpan{ a } == Mm::ElemsSpan{ b });
		CHECK_FALSE(Mm::ElemsSpan{ a } < Mm::ElemsSpan{ b });
		CHECK_FALSE(Mm::ElemsSpan{ a } <= Mm::ElemsSpan{ b });
	}
	SECTION("Different size, smaller empty")
	{
		std::array<Mm::sz, 4> a{1, 2, 3, 4};
		std::array<Mm::sz, 0> b{};
		CHECK(Mm::ElemsSpan{ a } != Mm::ElemsSpan{ b });
		CHECK(Mm::ElemsSpan{ a } > Mm::ElemsSpan{ b });
		CHECK(Mm::ElemsSpan{ a } >= Mm::ElemsSpan{ b });
		CHECK_FALSE(Mm::ElemsSpan{ a } == Mm::ElemsSpan{ b });
		CHECK_FALSE(Mm::ElemsSpan{ a } < Mm::ElemsSpan{ b });
		CHECK_FALSE(Mm::ElemsSpan{ a } <= Mm::ElemsSpan{ b });
	}
}

TEST_CASE("Clifford Algebra evaluates correctly", "[Mm]")
{
	// Multivector{3, 1, MVector{3, Int64}}([v1, v2, v3])
	SECTION("Basic operations evaluate")
	{
		CHECK(to_string(e1) == "e₁");  					// BasisBlade{3, 1}(1, 0b001)
		CHECK(to_string(-e1) == "-e₁");  				// BasisBlade{3, 1}(-1, 0b001)
		CHECK(to_string(-(-e1)) == "e₁");  				// BasisBlade{3, 1}(1, 0b001)
		CHECK(to_string(e1 + e1) == "2e₁");  			// Multivector{3, 1, MVector{3, Int64}}([2, 0, 0])
		CHECK(to_string(e1 + e2) == "e₁+e₂");  			// Multivector{3, 1, MVector{3, Int64}}([1, 1, 0])
		CHECK(to_string(e1 - e1) == "");  				// Multivector{3, 1, MVector{3, Int64}}([0, 0, 0])
		CHECK(to_string(e1 * e1) == "1");  				// BasisBlade{3, 0}(1, 0b000)
		CHECK(to_string(e2 * e2) == "1");  				// BasisBlade{3, 0}(1, 0b000)
		CHECK(to_string(e3 * e3) == "1");  				// BasisBlade{3, 0}(1, 0b000)
		CHECK(to_string(e1 * e2) == "e₁e₂");  			// BasisBlade{3, 2}(1, 0b011)
		CHECK(to_string(e2 * e1) == "-e₁e₂");  			// BasisBlade{3, 2}(-1, 0b011)
		CHECK(to_string(e1 * (e1 + e2)) == "1+e₁e₂");  	// Multivector{3, 0:2:2, MVector{4, Int64}}([1, 1, 0, 0])
		CHECK(to_string(e2 * (e1 + e2)) == "1-e₁e₂");  	// Multivector{3, 0:2:2, MVector{4, Int64}}([1, -1, 0, 0])
		CHECK(to_string(e2 * e1 + e2 * e2) == "1-e₁e₂");// Multivector{3, 0:2:2, MVector{4, Int64}}([1, -1, 0, 0])
	}
	// Multivector{3, 0:2:2, MVector{4, Int64}}([Z, v12, v13, v23])
	SECTION("Compound operations evaluate")
	{
		CHECK(to_string(a * a) == "-1");  				// BasisBlade{3, 0}(-1, 0b000)
		CHECK(to_string(b * b) == "-1");  				// BasisBlade{3, 0}(-1, 0b000)
		CHECK(to_string(a * b) == "-e₁e₃");  			// BasisBlade{3, 2}(-1, 0b101)
		CHECK(to_string(b * a) == "e₁e₃");  			// BasisBlade{3, 2}(1, 0b101)
		CHECK(to_string(a * (a + b)) == "-1-e₁e₃");  	// Multivector{3, 0:2:2, MVector{4, Int64}}([-1, 0, -1, 0])
		CHECK(to_string(b * (a + b)) == "-1+e₁e₃");  	// Multivector{3, 0:2:2, MVector{4, Int64}}([-1, 0, 1, 0])
		CHECK(to_string((a + b) * a) == "-1+e₁e₃");  	// Multivector{3, 0:2:2, MVector{4, Int64}}([-1, 0, 1, 0])
		CHECK(to_string((a + b) * b) == "-1-e₁e₃");  	// Multivector{3, 0:2:2, MVector{4, Int64}}([-1, 0, -1, 0])
		CHECK(to_string((c + b) * c) == "");  			// Multivector{3, 0:2:2, MVector{4, Int64}}([0, 0, 0, 0])
		CHECK(to_string((c + b) * b) == "");  			// Multivector{3, 0:2:2, MVector{4, Int64}}([0, 0, 0, 0])
		CHECK(to_string(lhs + rhs) == "-2");  			// Multivector{3, 0:2:2, MVector{4, Int64}}([-2, 0, 0, 0])
		CHECK(to_string((a + b) * (a + b)) == "-2");  	// Multivector{3, 0:2:2, MVector{4, Int64}}([-2, 0, 0, 0])
		CHECK(to_string(a * b) == "-e₁e₃");  			// BasisBlade{3, 2}(-1, 0b101)
		CHECK(to_string(a) == "e₂e₃");  				// BasisBlade{3, 2}(1, 0b110)
		CHECK(to_string(b) == "e₁e₂");  				// BasisBlade{3, 2}(1, 0b011)
		CHECK(to_string(c) == "-e₁e₂");  				// BasisBlade{3, 2}(-1, 0b011)
	}
}